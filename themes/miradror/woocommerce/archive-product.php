<?php

/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined('ABSPATH') || exit;


get_header('shop');

/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */


/*do_action( 'woocommerce_before_main_content' );
				<?php echo do_shortcode('[woof]')?>
*/

?>
<header class="woocommerce-products-header">
    <?php

        global $wp_query;

        // get the query object
        $cat = $wp_query->get_queried_object();
        $cat_type = $cat->taxonomy;
        $cat_id = $cat->term_id;

        $img_url = get_field('category_image', $cat_type.'_'.$cat_id);

        if ($img_url == '') { 

            $img_url = get_field('category_image', 'product_cat_'.$cat_id);

        }
    
    ?>
    
    <div class="container top cat-head">
    	<img class='back-category d-none d-md-block' src='<?php echo $img_url ?>' alt=''/>
        <div class="row">
            <div class="col-12 col-md-8 col-lg-5">
                <div class="cat-breadcrumb">
                    <?php woocommerce_breadcrumb(); ?>
                </div>
                <div class="d-block d-md-none breadcrumb-sm">
                    <a onclick="goBack()"><i class="fas fa-chevron-left"></i> Volver</a>
                </div>
                <div class="cat-content">
                    <?php if (apply_filters('woocommerce_show_page_title', true)) : ?>
                    <h1 class="woocommerce-products-header__title page-title">
                        <?php
                            woocommerce_page_title();
                            //foreach ($_GET as $key => $value) echo "Key: $key Val: $value<br>";
                            ?>
                    </h1>
                    <?php endif; ?>
                    <?php
                    the_archive_description('<div class="taxonomy-description">', '</div>');
                    ?>
                </div>
            </div>
        </div>
    </div>
</header>
<section>
    <div class="container d-none d-md-block">
        <div class="row">
            <?php 
            get_template_part('pages/sections/sliders/product', 'slider'); ?>
        </div>
    </div>

    <div class="d-block d-md-none nav-filters">
        <nav id="myNavmenu" class="navmenu navmenu-default navmenu-fixed-left offcanvas" role="navigation">
            <!-- FILTROS -->
            <?php //echo do_shortcode('[searchandfilter id="653"]'); ?>

            <?php
            //Produccion
            echo do_shortcode('[searchandfilter id="834"]'); ?>

        </nav>
        <div class="navbar d-block d-md-none d-flex justify-content-center">
            <button class="btn btn-filter" type="button" data-toggle="offcanvas" data-target="#myNavmenu" data-canvas="body">
                <p><i class="fas fa-sort-amount-down"></i> FILTRAR</p>
            </button>
            <button class="btn btn-filter-last" type="button">
                <i class="fas fa-align-justify"></i> ORDENAR
            </button>

        </div>
    </div>

    <div class="container border-category">
        <div class="row">
            <div class="col-md-6 d-none d-md-block">
                <h2>Todos los productos</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-3 check-fiters">
                <?php //echo do_shortcode('[searchandfilter id="653"]'); ?>

                <?php
                //Produccion
                 echo do_shortcode('[searchandfilter id="834"]'); ?>

            </div>
            <div class="col-12 col-lg-9 product-loop" id="product-loop">
                <?php
                if (woocommerce_product_loop()) {
                    //woocommerce_product_loop_start();
                    if (wc_get_loop_prop('total')) {
                        $i=0;
                        while (have_posts()) {
                        the_post();
                        if($i==0){ ?>
                            <div class="row">
                        
                        <?php $i=1; }
                        
                          else if ($i==4) { ?>
                        <!--CIERRO EL ROW -->
                        </div>
                        <div class="row">
                        
                        <?php $i=1; } ?>
                        
                        <div class="col-sm-6 col-lg-4 mb-3"> <?php
                        get_template_part('woocommerce/inc/cards/product', 'card');
                        ?> </div><?php
                        $i++;
                        }
                        }
                          } else {
                                do_action('woocommerce_no_products_found');
                          }
                        ?>
            </div>
        </div>
        <div class="container d-block d-md-none">
            <div class="row">
                <?php get_template_part('pages/sections/sliders/product', 'slider'); ?>
            </div>
        </div>
    </div>
</section>

<?php get_template_part('woocommerce/inc/help'); ?>
<?php

get_footer('shop');
?>