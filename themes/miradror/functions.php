<?php
/**
 * Understrap functions and definitions
 *
 * @package understrap
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

$understrap_includes = array(
    '/acf.php',
    '/theme-settings.php',                  // Initialize theme default settings.
    '/setup.php',                           // Theme setup and custom theme supports.
    '/widgets.php',                         // Register widget area.
    '/enqueue.php',                         // Enqueue scripts and styles.
    '/template-tags.php',                   // Custom template tags for this theme.
    '/pagination.php',                      // Custom pagination for this theme.
    '/hooks.php',                           // Custom hooks.
    '/extras.php',                          // Custom functions that act independently of the theme templates.
    '/customizer.php',                      // Customizer additions.
    '/custom-comments.php',                 // Custom Comments file.
    '/jetpack.php',                         // Load Jetpack compatibility file.
    '/class-wp-bootstrap-navwalker.php',    // Load custom WordPress nav walker.
    '/woocommerce.php',                     // Load WooCommerce functions.
    '/editor.php',                          // Load Editor functions.
    '/deprecated.php',                      // Load deprecated functions.
);

foreach ($understrap_includes as $file) {
    $filepath = locate_template('inc' . $file);
    if (!$filepath) {
        trigger_error(sprintf('Error locating /inc%s for inclusion', $file), E_USER_ERROR);
    }
    require_once $filepath;
}

function register_tax_taxonomy()
{
    $tax_labels = array(
        'name' => 'Propiedades',
        'singular_name' => 'Propiedad',
        'search_items' => 'Buscar',
        'popular_items' => '',
        'all_items' => 'Toda',
        'parent_item' => 'Propiedad parent',
        'parent_item_colon' => 'Propiedad parent:',
        'edit_item' => 'Editar Propiedad',
        'update_item' => 'Actualizar Propiedad',
        'add_new_item' => 'Agregar Propiedad',
        'new_item_name' => 'Nuevo nombre de Propiedad',
        'separate_items_with_commas' => 'Separar Propiedads con comas',
        'add_or_remove_items' => 'Agregar o borrar Propiedad',
        'choose_from_most_used' => 'Elegir entre las Propiedades más usadas',
        'menu_name' => 'Propiedades'
    );

    register_taxonomy(
        'propiedad',
        array('product'),
        array(
            'hierarchical' => true,
            'show_ui' => true,
            'labels' => $tax_labels,
            'public' => true,
            'query_var' => true,
            'label' => 'Taxonomias',
            'rewrite' => array('slug' => 'propiedad', 'hierarchical' => true),
            'taxonomies' => array('post_tag')
        )
    );
}

add_action('init', 'register_tax_taxonomy');

$tax_name = 'Tipo de Piel';
$tax_singular_name = 'Tipo de Piel';
$tax_plural_name = 'Tipo de Piel';

function register_tax_taxonomy2()
{
    $tax_labels = array(
        'name' => 'Tipo de Piel',
        'singular_name' => 'Tipo de Piel',
        'search_items' => 'Buscar',
        'popular_items' => '',
        'all_items' => 'Toda',
        'parent_item' => 'Tipo de Piel parent',
        'parent_item_colon' => 'Tipo de Piel parent:',
        'edit_item' => 'Editar Tipo de Piel',
        'update_item' => 'Actualizar Tipo de Piel',
        'add_new_item' => 'Agregar Tipo de Piel',
        'new_item_name' => 'Nuevo nombre de Tipo de Piel',
        'separate_items_with_commas' => 'Separar Tipo de Piels con comas',
        'add_or_remove_items' => 'Agregar o borrar Tipo de Piel',
        'choose_from_most_used' => 'Elegir entre las Tipo de Piel más usadas',
        'menu_name' => 'Tipo de Piel'
    );

    register_taxonomy(
        'tipo_de_piel',
        array('product'),
        array(
            'hierarchical' => true,
            'show_ui' => true,
            'labels' => $tax_labels,
            'public' => true,
            'query_var' => true,
            'label' => 'Taxonomias',
            'rewrite' => array('slug' => 'tipo-de-piel', 'hierarchical' => true),
            'taxonomies' => array('post_tag')
        )
    );
}

add_action('init', 'register_tax_taxonomy2');

$tax_name = 'Tipo de Producto';
$tax_singular_name = 'Tipo de Producto';
$tax_plural_name = 'Tipo de Producto';

function register_tax_taxonomy3()
{
    $tax_labels = array(
        'name' => 'Tipo de Producto',
        'singular_name' => 'Tipo de Producto',
        'search_items' => 'Buscar',
        'popular_items' => '',
        'all_items' => 'Toda',
        'parent_item' => 'Tipo de Producto parent',
        'parent_item_colon' => 'Tipo de Producto parent:',
        'edit_item' => 'Editar Tipo de Producto',
        'update_item' => 'Actualizar Tipo de Producto',
        'add_new_item' => 'Agregar Tipo de Producto',
        'new_item_name' => 'Nuevo nombre de Tipo de Producto',
        'separate_items_with_commas' => 'Separar Tipo de Productos con comas',
        'add_or_remove_items' => 'Agregar o borrar Tipo de Producto',
        'choose_from_most_used' => 'Elegir entre las Tipo de Producto más usadas',
        'menu_name' => 'Tipo de Producto'
    );

    register_taxonomy(
        'tipo_de_producto',
        array('product'),
        array(
            'hierarchical' => true,
            'show_ui' => true,
            'labels' => $tax_labels,
            'public' => true,
            'query_var' => true,
            'label' => 'Taxonomias',
            'rewrite' => array('slug' => 'tipo-de-producto', 'hierarchical' => true),
            'taxonomies' => array('post_tag')
        )
    );
}

add_action('init', 'register_tax_taxonomy3');

/*function register_my_menu() {
    register_nav_menu('header-menu',__( 'Header Menu' )); }
add_action( 'init', 'register_my_menu' );*/

if (function_exists('register_nav_menu')) {
    register_nav_menu('header_menu', 'Header Menu');
}


add_action('wp_ajax_myfilter', 'misha_filter_function');
add_action('wp_ajax_nopriv_myfilter', 'misha_filter_function');

function misha_filter_function()
{
    $args = array(
        'orderby' => 'date',
        'order' => $_POST['date']
    );
    if (isset($_POST['categoryfilter']))
        $args['tax_query'] = array(
            array(
                'taxonomy' => 'product_cat',
                'field' => 'id',
                'terms' => $_POST['categoryfilter']
            )
        );

    $query = new WP_Query($args);

    if ($query->have_posts()) :
        while ($query->have_posts()) : $query->the_post();
            echo '<h2>' . $query->post->post_title . '</h2>';
        endwhile;
        wp_reset_postdata();
    else :
        echo 'No posts found';
    endif;

    die();
}

function wpdocs_custom_excerpt_length($length)
{
    return 18;
}

add_filter('excerpt_length', 'wpdocs_custom_excerpt_length', 999);

function woocommerce_maybe_add_multiple_products_to_cart()
{
    // Make sure WC is installed, and add-to-cart qauery arg exists, and contains at least one comma.
    if (!class_exists('WC_Form_Handler') || empty($_REQUEST['add-to-cart']) || false === strpos($_REQUEST['add-to-cart'], ',')) {
        return;
    }

    // Remove WooCommerce's hook, as it's useless (doesn't handle multiple products).
    remove_action('wp_loaded', array('WC_Form_Handler', 'add_to_cart_action'), 20);

    $product_ids = explode(',', $_REQUEST['add-to-cart']);
    $count = count($product_ids);
    $number = 0;

    foreach ($product_ids as $product_id) {
        if (++$number === $count) {
            // Ok, final item, let's send it back to woocommerce's add_to_cart_action method for handling.
            $_REQUEST['add-to-cart'] = $product_id;

            return WC_Form_Handler::add_to_cart_action();
        }

        $product_id = apply_filters('woocommerce_add_to_cart_product_id', absint($product_id));
        $was_added_to_cart = false;
        $adding_to_cart = wc_get_product($product_id);

        if (!$adding_to_cart) {
            continue;
        }

        $add_to_cart_handler = apply_filters('woocommerce_add_to_cart_handler', $adding_to_cart->product_type, $adding_to_cart);

        /*
         * Sorry.. if you want non-simple products, you're on your own.
         *
         * Related: WooCommerce has set the following methods as private:
         * WC_Form_Handler::add_to_cart_handler_variable(),
         * WC_Form_Handler::add_to_cart_handler_grouped(),
         * WC_Form_Handler::add_to_cart_handler_simple()
         *
         * Why you gotta be like that WooCommerce?
         */
        if ('simple' !== $add_to_cart_handler) {
            continue;
        }

        // For now, quantity applies to all products.. This could be changed easily enough, but I didn't need this feature.
        $quantity = empty($_REQUEST['quantity']) ? 1 : wc_stock_amount($_REQUEST['quantity']);
        $passed_validation = apply_filters('woocommerce_add_to_cart_validation', true, $product_id, $quantity);

        if ($passed_validation && false !== WC()->cart->add_to_cart($product_id, $quantity)) {
            wc_add_to_cart_message(array($product_id => $quantity), true);
        }
    }
}


// Fire before the WC_Form_Handler::add_to_cart_action callback.
add_action('wp_loaded', 'woocommerce_maybe_add_multiple_products_to_cart', 15);

add_action('after_setup_theme', 'remove_admin_bar');

function remove_admin_bar()
{
    if (!current_user_can('administrator') && !is_admin()) {
        show_admin_bar(false);
    }
}


add_action('wp_enqueue_scripts', 'dcms_insertar_js');

function dcms_insertar_js(){

    if (!is_home()) return;

    wp_register_script('dcms_miscript', get_template_directory_uri(). '/js/theme.min.js', array('jquery'), '1', true );
    wp_enqueue_script('dcms_miscript');

    wp_localize_script('dcms_miscript','dcms_vars',['ajaxurl'=>admin_url('admin-ajax.php')]);
}


function get_ajax_posts()
{
    $term = $_POST['term'];

    if ($term!='') {
        $tax_query[] = array(
            'relation' => 'AND',
            array(
                'taxonomy' => 'product_visibility',
                'field' => 'name',
                'terms' => 'featured',
                'operator' => 'IN', // or 'NOT IN' to exclude feature products
            ),
            array(
                'taxonomy' => 'product_cat',
                'field' => 'slug',
                'terms' => $term
            )
        );
        // The query
        $args = array(
            'post_type' => 'product',
            'order' => 'DES',
            'orderby' => 'menu_order',
            'posts_per_page' => 8,
            'tax_query' => $tax_query // <===
        );
    }else {
        // The query
        $args = array(
            'post_type' => 'product',
            'order' => 'DES',
            'orderby' => 'menu_order',
            'posts_per_page' => 8,
            'taxonomy' => 'product_visibility',
                'field' => 'name',
                'terms' => 'featured',
        );
    }


    // The Query
    $ajaxposts = new WP_Query($args);

    $response = '';

    // The Query
    if ($ajaxposts->have_posts()) {
        while ($ajaxposts->have_posts()) {
            $ajaxposts->the_post();
            $response .= get_template_part('woocommerce/inc/cards/product', 'card');
        }
    } else {
        $response .= 'No hay productos destacados';
    }

    echo $response;

    exit; // leave ajax call
}

// Add term and conditions check box on registration form
add_action( 'woocommerce_register_form', 'add_terms_and_conditions_to_registration', 20 );
function add_terms_and_conditions_to_registration() {

    if ( wc_get_page_id( 'terms' ) > 0 && is_account_page() ) {
        ?>
        <p class="form-row terms wc-terms-and-conditions">
            <label class="woocommerce-form__label woocommerce-form__label-for-checkbox checkbox">
                <input type="checkbox" class="woocommerce-form__input woocommerce-form__input-checkbox input-checkbox" name="terms" <?php checked( apply_filters( 'woocommerce_terms_is_checked_default', isset( $_POST['terms'] ) ), true ); ?> id="terms" /> <span><?php printf( __( 'He leído y estoy de acuerdo con los <a href="%s" target="_blank" class="woocommerce-terms-and-conditions-link">términos y condiciones</a>', 'woocommerce' ), esc_url( wc_get_page_permalink( 'terms' ) ) ); ?></span> <span class="required">*</span>
            </label>
            <input type="hidden" name="terms-field" value="1" />
        </p>
    <?php
    }
}

// Validate required term and conditions check box
add_action( 'woocommerce_register_post', 'terms_and_conditions_validation', 20, 3 );
function terms_and_conditions_validation( $username, $email, $validation_errors ) {
    if ( ! isset( $_POST['terms'] ) )
        $validation_errors->add( 'terms_error', __( 'Términos y condiciones no aceptados!', 'woocommerce' ) );

    return $validation_errors;
}

// Fire AJAX action for both logged in and non-logged in users
add_action('wp_ajax_get_ajax_posts', 'get_ajax_posts');
add_action('wp_ajax_nopriv_get_ajax_posts', 'get_ajax_posts');

add_action('wp_enqueue_scripts', 'dcms_insertar_js');


