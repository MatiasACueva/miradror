<div class="container">

    <div class="row">
        <div class="col-lg-5 text-center text-lg-left">
            <h2>Productos Destacados</h2>
        </div>
        <div class="col-lg-7">
            <!-- Filter Featured product by Category -->
            <form class="filter" action="<?php echo site_url() ?>/wp-admin/admin-ajax.php" method="POST" id="filter">
                <?php if ($terms = get_terms(array(
                    'taxonomy' => 'product_cat',
                    'orderby' => 'name',
                    'parent' => '0'
                ))) :
                    $i = 1;
                    $total = count($terms);
                    ?>

                <div class="d-none d-md-block">
                    <?php
                        foreach ($terms as $term) : ?>
                    <?php $name = $term->name; ?>
                    <a class="btn filter-btn <?php if ($i == 1) : echo 'last'; endif; ?>" href="#" id="<?php echo $term->term_id ?>"><?php echo $name ?></a>
                    <?php ++$i;
                        endforeach; ?>
                </div>

                <div class="d-block d-md-none swiper-featured">
                    <div class="swiper-wrapper">
                        <?php
                            foreach ($terms as $term) : ?>
                        <?php $name = $term->name; ?>
                        <a class="btn filter-btn swiper-slide<?php if ($i == $total) : echo 'last';
                                                                        endif; ?>" href="<?php echo site_url() ?>/categorias/<?php echo $name  ?>" value="<?php echo $term->term_id ?>"><?php echo $name ?></a>
                        <?php ++$i;
                            endforeach; ?>
                    </div>
                </div>

                <?php endif; ?>
                <input type="hidden" name="action" value="myfilter">
            </form> <!-- End Filter Featured product by Category -->
        </div>
        <?php

        set_query_var('term', $term);

        //print_r($term);?>
        <div class="ajax-content w-100">
            <?php get_template_part('pages/sections/sliders/product', 'slider'); ?>
        </div>
    </div>
</div>
