<?php /* Template Name: contact */ ?>

<?php get_header( 'shop' ); ?>
<section id="contact">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6">
                    <h5>Completa el formulario de contacto o comunicate con nosotros por cualquiera de estos medios:</h5>
                    <p><i class="far fa-envelope"></i> Email: <a href="mailto:">hola@miradror.com.ar </a></p>
                    <p><i class="fas fa-phone"></i> Telefono: <a href="#">0800 333 8536</a></p>
                    <p><i class="fab fa-whatsapp"></i> Whatsapp: <a href="#">11 5003584</a></p>
            </div>
            <div class="col-12 col-md-6">
                <form id="contact-form" action="<?php echo get_template_directory_uri() ?>/email.php" method="post" data-abide='ajax' data-toggle="validator" role="form" class="needs-validation" novalidate>
                    <fieldset>
                        <div class="form-group">
                            <!-- <p>NOMBRE</p> -->
                            <input type="text" name="name" class="form-control border" id="form_name" placeholder="Nombre" required>
                            <div class="help-block with-errors"></div>
                            <div class="invalid-feedback">
                            <p class="invalid">Ingresá tu nombre</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <!-- <p>TELÉFONO</p> -->
                            <input type="text" name="phone" class="form-control border" id="form_phone" placeholder="Teléfono" required>
                            <div class="help-block with-errors"></div>
                            <div class="invalid-feedback">
                            <p class="invalid">Ingresá tu teléfono</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <!-- <p>EMAIL</p> -->
                            <input type="email" name="email" class="form-control border" id="form_email" placeholder="E-mail" required>
                            <div class="help-block with-errors"></div>
                            <div class="invalid-feedback">
                                <p class="invalid">Ingresá tu email</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <!-- <p>MENSAJE</p> -->
                            <textarea name="mensaje" class="form-control border" id="form_message" cols="30" rows="5" placeholder="Mensaje" required></textarea>
                            <div class="help-block with-errors"></div>
                            <div class="invalid-feedback">
                            <p class="invalid">Ingresá un mensaje</p> 
                            </div>
                        </div>
                        <div id="messages" class="success">
                        </div>
                        <div class="form-group">
                            <input id="submit" type="submit" class="btn btn-primary float-right" value="ENVIAR">
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>