<?php /* Template Name: sellpoints */ ?>

<?php get_header( 'shop' ); ?>
<div class="container">
    
    <div class="sellpoint-title">
        <h1>Bienvenido al buscador de la nomina de profesionales Avaladas por Laboratorio Mira Dror.</h1>
    </div>
    <h5>Estos profesionales son los que te pueden brindar el más preciso diagnóstico de piel, el mejor asesoramiento profesional personalizado y son ellos los que te pueden ofrecer los productos Mira Dror más adecuados para vos en combinación con el tratamiento que más precisa tu piel.</h5>

    <div class="my-1">
        <?php echo do_shortcode('[wp_simple_locator 
                                    distances="50"
                                    buttontext="Buscar" 
                                    addresslabel="Ingresar tu ciudad"
                                    mapcontrols="show" 
                                    mapcontrolsposition="RIGHT_BOTTOM"
                                    placeholder=" Ciudad"] ') ?>
    </div>

</div>
<div class='clearfix'></div>


<?php get_footer( 'shop' ); ?>
