<section id="categories" class="spacer categories d-none d-lg-block">
    <div class="container-fluid categories-backgrounds">
        <div class="row">
            <div class="col-6 categories-man">
                <img src="<?php echo get_template_directory_uri() ?>/assets/img/back-blue.png" alt="">
            </div>
            <div class="col-6 categories-young">
            </div>
        </div>
    </div>
    <div class="container categories-content">
        <div class="row">
            <div class="col-6 categories-man">
                <div class="row">
                    <div class="col-6">                   
                        <h3>Hombres</h3>
                        <p>La cosmética no entiende de sexos solo busca el cuidado, la higiene y la protección del cuerpo. Muchas marcas se centran siempre toda su atención en el neceser femenino [...]</p>
                        <a href="<?php echo home_url() ?>/consejos-para-pieles-deshidratas/">CONOCE MÁS</a>
                    </div> 
                    <div class="col-6 align-self-end">
                        <img src="<?php echo get_template_directory_uri() ?>/assets/img/Destacados_hombres_2.png" alt="">
                    </div>
                </div>
            </div>
            <div class="col-6 categories-young">
                <div class="row">
                    <div class="col-6 ">
                        <h3>Jovenes</h3>
                        <p>La belleza fresca y liviana de una piel sana es una tendencia global que llegó para quedarse. La vuelta a lo esencial y la vida sana se traducen [...]</p>
                        <a href="<?php echo home_url() ?>/9-cremas-hidratantes-para-usar-a-partir-de-los-20-anos">CONOCE MÁS</a>
                    </div>
                    <div class="col-6 align-self-end">
                        <img src="<?php echo get_template_directory_uri() ?>/assets/img/destacados_jovenes_3.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="categories-sm d-block d-lg-none">
    <div class="container">
            <div class="swiper-category">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                            <div class="card">
                                <img src="<?php echo get_template_directory_uri() ?>/assets/img/Destacados_hombres_2.png" alt="">
                                <div class="card-body card-man">
                                    <h3 class="card-title card-man">Hombres</h3>
                                    <p class="card-text pb-3">La cosmética no entiende de sexos solo busca el cuidado, la higiene y la protección del cuerpo. [...]</p>
                                    <div class="cat-button">
                                        <a href="<?php home_url() ?> ?>/consejos-para-pieles-deshidratas/">CONOCE MÁS</a>
                                    </div>                         
                                </div>
                            </div>
                    </div>
                    <div class="swiper-slide">
                            <div class="card">
                                <a href="#"><img src="<?php echo get_template_directory_uri() ?>/assets/img/destacados_jovenes_3.png" alt=""></a>
                                <div class="card-body card-young">
                                    <h3 class="card-title">Jovenes</h3>
                                    <p class="card-text pb-3">La belleza fresca y liviana de una piel sana es una tendencia global que llegó para quedarse. [...]</p>
                                    <div class="cat-button">
                                        <a href="<?php echo get_template_directory_uri() ?>/assets/img/destacados_jovenes_3.png">CONOCE MÁS</a>
                                    </div>                         
                                </div>
                            </div>
                    </div>
                </div>
                <div class="swiper-pagination-cat"></div>
            </div>
    </div>
</section>