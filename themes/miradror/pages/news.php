<?php /* Template Name: news */ ?>

<?php get_header( 'shop' ); ?>

<?php if ( have_posts() ) {
	while ( have_posts() ) {
        the_post(); 
        
?>

<!-- title -->

<section>
    <div class="back-news">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12 col-md-6">
                    <h2>NOVEDADES</h2>
                    <p><?php the_content()?></p>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- end title -->

<!-- news -->

<section class="news">
    <div class="news-posts">
        <div class="container">
            <div class="row">
                <?php   
                                $args = array(
                                'post_type' => 'post',
                                'posts_per_page' => -1,
                                'post_parent' => 0,
                                'order' => 'DES',
                                'orderby' => 'menu_order'
                                            );
                            
                                query_posts($args);

                                if ( have_posts() ):
                                while ( have_posts() ):
                                    the_post(); 
                                    ?>
                                    <div class="col-md-6 col-lg-4"> 

                                    <?php get_template_part('woocommerce/inc/cards/news','card'); ?>

                                    </div>
                                    
                                    <?php
                                    // display a sub field value
                                    

                                endwhile;

                            else :

                                // no rows found

                            endif;

                            ?>
                            <?php wp_reset_query() ?>
            </div>
        </div>
    </div>
</section>

<!-- end news -->
<?php }} ?>
<?php get_footer(); ?>