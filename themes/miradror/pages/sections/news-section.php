<section id="news">
    <div class="container news">
        <div class="row">
            <div class="col-12 text-center text-md-left">
                <h2 class="float-left">Conoce Más</h2>
                <a class="all-news" href="<?php echo home_url() ?>/novedades">VER TODOS</a>
            </div>
            <div class="d-none d-lg-block padding-slider-xl">
                <div class="row">
                    <?php
                    $args = array(
                        'post_type' => 'post',
                        'posts_per_page' => 3,
                        'post_parent' => 0,
                        'order' => 'ASD',
                    );

                    query_posts($args);

                    if (have_posts()) :
                        while (have_posts()) :
                            the_post();
                            ?>
                    <div class="col-4">
                        <?php
                                get_template_part('woocommerce/inc/cards/news', 'card');
                                ?>
                    </div>
                    <?php
                        // display a sub field value


                        endwhile;
                        ?>
                    <?php endif;
                    wp_reset_query() ?>
                </div>

            </div>
            <div class="swiper-news d-block d-lg-none">
                <div class="swiper-wrapper">
                    <?php
                    $args = array(
                        'post_type' => 'post',
                        'posts_per_page' => 3,
                        'post_parent' => 0,
                        'order' => 'ASD',
                        'orderby' => 'menu_order'
                    );

                    query_posts($args);

                    if (have_posts()) :
                        while (have_posts()) :
                            the_post();


                            get_template_part('woocommerce/inc/cards/news', 'card');

                        // display a sub field value


                        endwhile;

                    else :

                    // no rows found

                    endif;

                    ?>
                    <?php wp_reset_query() ?>
                </div>
                <div class="swiper-pagination-news"></div>
            </div>
        </div>
    </div>
</section>