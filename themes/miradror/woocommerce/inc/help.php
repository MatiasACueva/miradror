<section id="help">
  <div class="help-specifications">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-10 col-md-4 align-self-center text-center text-md-left">
          <h4 class="text-primary">¿Tenés dudas?</h4>
          <p class="comuncation">Comunicate con nosotros y un asesor contestará todas tus preguntas</p>
					<p><a href="tel:+5408003338536" target="_blank"><i class="fas fa-phone"></i> 0800 333 8536</a></p>
					<p><a class="whatsapp-lg" href="https://web.whatsapp.com/send?phone=+549115003584" target="_blank"><i class="fab fa-whatsapp"></i> 11 5003584</a></p>
					<p><a class="whatsapp-sm" href="whatsapp://send?phone=+549115003584" target="_blank"><i class="far fa-envelope"></i> hola@miradror.com.ar</a></p>
          <a href="<?php echo home_url(); ?>/contacto/" class="btn btn-primary">CONSULTANOS</a>
        </div>
        <div class="col-md-4 d-none d-md-block">
          <img src="<?php echo get_template_directory_uri() ?>/assets/img/consultas_2.png" alt="">
        </div>
      </div>
    </div>
  </div>
</section>