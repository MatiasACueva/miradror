<?php

/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package understrap
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

get_header();

$container = get_theme_mod('understrap_container_type');
?>

<!-- home-carrousel Section -->

<?php get_template_part('pages/sections/home', 'carrousel'); ?>

<!-- End home-carrousel Section -->


<!-- featured-products Section -->

<div class="container">

    <div class="row">
        <div class="col-lg-4 text-center text-lg-left">
            <h2>Productos Destacados</h2>
        </div>
        <div class="col-lg-8">
            <!-- Filter Featured product by Category -->
            <div class="filter" id="filter">
                <?php if ($terms = get_terms(array(
                    'taxonomy' => 'product_cat',
                    'orderby' => 'name',
                    'parent' => '0'
                ))) :
                    $i = 1;
                    $total = count($terms);
                    ?>

                <div class="d-none d-xl-block">

                    <?php
                        foreach ($terms as $term) : ?>
                    <?php $name = $term->name; ?>
                    <a class="btn filter-btn <?php if ($i == 1) : echo 'last';
                                                        endif; ?>" href="#" data-filter="<?php echo $term->slug; ?>" id="<?php echo $term->term_id ?>"><?php echo $name ?></a>
                    <?php ++$i;
                        endforeach; ?>
                    <a class="btn filter-btn active">
                        TODOS
                    </a>
                </div>

                <div class="d-block d-xl-none swiper-featured">
                    <div class="swiper-wrapper">
                        <a class="btn filter-btn swiper-slide active">
                            TODOS
                        </a>
                        <?php
                            $i = 1;
                            foreach ($terms as $term) : ?>
                        <?php $name = $term->name; ?>
                        <a class="btn  filter-btn swiper-slide <?php if ($i == $total) : echo 'last-cat';
                                                                        endif; ?>" href="#" data-filter="<?php echo $term->slug; ?>"><?php echo $name ?></a>
                        <?php ++$i;
                            endforeach; ?>
                    </div>
                </div>

                <?php endif; ?>
            </div> <!-- End Filter Featured product by Category -->
        </div>
        <?php

        //print_r($term);
        ?>
        <div class="ajax-content w-100">
            <?php
            // The tax query
            // Traemmos todos los destacados
            $args = array(
                'post_type' => 'product',
                'order' => 'DES',
                'orderby' => 'menu_order',
                'posts_per_page' => 8,
                'taxonomy' => 'product_visibility',
                'field' => 'name',
                'terms' => 'featured',
            );


            ?>
            <div class="padding-slider-xl">
                <div class="swiper-container">
                    <div class="swiper-wrapper" id="ajax-content">
                        <?php
                        query_posts($args);

                        if (have_posts()) :
                            $count = $wp_query->post_count;
                            while (have_posts()) : the_post();
                                get_template_part('woocommerce/inc/cards/product', 'card');
                            endwhile;
                        endif;
                        ?>
                    </div> <!-- End Swiper Wrapper -->

                    <div class="swiper-pagination"></div>
                </div>
            </div>
            <?php wp_reset_query() ?>
        </div>
    </div>
</div>

<?php /*get_template_part('pages/sections/filtro');*/ ?>

<!-- End featured-products Section -->


<!-- Categories Section -->

<?php get_template_part('pages/sections/categories'); ?>

<!-- End Categories Section -->


<!-- Best-sellers Section -->

<?php get_template_part('pages/sections/best', 'sellers'); ?>

<!-- End Best-sellers Section -->


<!-- Laboratory Section -->

<?php get_template_part('pages/sections/laboratory'); ?>

<!-- End Laboratory Section -->

<!-- News Section -->

<?php get_template_part('pages/sections/news', 'section'); ?>

<!-- End News Section -->


<?php get_footer(); ?>