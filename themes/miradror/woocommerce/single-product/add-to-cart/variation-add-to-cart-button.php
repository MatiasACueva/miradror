<?php
/**
 * Single variation cart button
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

global $product;
?>

<div class="woocommerce-variation-add-to-cart variations_button">
	<?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>

    <!-- PARA QUE ANDE EL AJAX TIENE QUE TENER LA CLASE single_add_to_cart_button -->

    <button type="submit" class="single_add_to_cart_button btn btn-primary text-uppercase d-block btn-cart">
        <span class="base">
            <?php echo esc_html($product->single_add_to_cart_text()); ?>
        </span>
        <span class="onloading">
              AGREGANDO AL CARRITO... <i class="fas fa-circle-notch fa-spin"></i>
        </span>
        <span class="adding">
              AGREGADO <i class="fas fa-check"></i>
        </span>
    </button>

	<?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>

	<input type="hidden" name="add-to-cart" value="<?php echo absint( $product->get_id() ); ?>" />
	<input type="hidden" name="product_id" value="<?php echo absint( $product->get_id() ); ?>" />
	<input type="hidden" name="variation_id" class="variation_id" value="0" />
</div>
