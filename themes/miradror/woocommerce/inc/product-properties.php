<!-- incluido en short-description -->

<div class="properties">
	<div class="properties-lg">
		<?php
		if (have_rows('product_properties')) :

			while (have_rows('product_properties')) : the_row();

				// display a sub field value
				$image = get_sub_field('product_properties_image');
				$name = get_sub_field('product_properties_name');
				?>
					<div class="properties-slide text-center">
						<div class="properties-img">
							<img src="<?php echo $image ?>" alt="">
						</div>
						<p class="text-primary">
							<?php echo $name ?>
						</p>
					</div>
				<?php
			endwhile;
		else :

		endif;
		?>
	</div>
	<div class="swiper-properties d-block d-md-none">
		<div class="swiper-wrapper">
			<?php
			if (have_rows('product_properties')) :

				while (have_rows('product_properties')) : the_row();

					// display a sub field value
					$image = get_sub_field('product_properties_image');
					$name = get_sub_field('product_properties_name');
					?>
			<div class="swiper-slide properties-slide text-center">
				<div class="properties-img">
					<img src="<?php echo $image ?>" alt="">
				</div>
				<p class="text-primary">
					<?php echo $name ?>
				</p>
			</div>
			<?php
				endwhile;
			else :

			endif;
			?>
		</div>
	</div>
</div>