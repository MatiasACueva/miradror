<div class="separator-nav d-none d-lg-block"></div>
<nav class="second-nav d-none d-lg-block">
    <div class="container">
        <div class="row">
            <ul class="nav nav-pills col-9">
                <?php if ($terms = get_terms(array(
                    'taxonomy' => 'product_cat',
                    'orderby' => 'name'
                ))) :
                    ?>
                <?php
                    foreach ($terms as $term) : ?>
                <?php $category_name = $term->slug; ?>
                <li class="nav-item megamenu-li">
                    <a class="nav-link dropdown-toggle text-uppercase cat-nav" data-toggle="dropdown" href="<?php echo site_url() ?>/categorias/<?php echo $term->name ?>" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $term->name ?></a>
                    <div class="megamenu">
                        <div class="container megamenu-cont">
                            <div class="row">
                                <div class="col-6 col-sm-3 column">
                                    <nav class="nav flex-column pl-0">
                                        <div class="nav-link mt-3"><b>PROPIEDADES</b></div>
                                        <?php
                                                // your taxonomy name
                                                $tax = 'propiedad';
                                                // get the terms of taxonomy
                                                $terms_under = get_terms($tax, $args = array(
                                                    'hide_empty' => false, // do not hide empty terms
                                                ));

                                                if (!empty($terms_under) && !is_wp_error($terms_under)) {
                                                    $i = 0;

                                                    // loop through all terms
                                                    foreach ($terms_under as $term_under) {
        
                                                        // Get the term link
                                                        $term_link = get_term_link($term_under);
                                                        //print_r(get_field('category_rel', 'propiedad_'.$term_under->term_id));
                                                        // if ($term_under->count > 0 and in_array($term->ID, get_field('category_rel'))) {
                                                        if (in_array($term->term_id, get_field('category_rel', $tax . '_' . $term_under->term_id))) {
                                                            if($i==10){
                                                                break;
                                                            }
                                                            // display link to term archive
                                                            echo '<a  class="nav-link color-hover" href="' . home_url() . '/categorias/' . $term->slug . '/?_sft_propiedad=' . $term_under->slug . '">' . $term_under->name . '</a>'; ?>
                                                            <?php
                                                        $i++;
                                                        }
                                                    }
                                                }
                                                ?>
                                        <a class="cat-see-all" href="<?php echo home_url() ?>/categorias/<?php echo $term->slug ?>">Ver todas <i class="fas fa-angle-right"></i></i></a>
                                    </nav>

                                </div>
                                <div class="col-6 col-sm-3 column">

                                    <nav class="nav flex-column pl-0">
                                        <div class="nav-link mt-3"><b>TIPO DE PIEL</b></div>
                                        <?php
                                                // your taxonomy name
                                                $tax = 'tipo_de_piel';

                                                // get the terms of taxonomy
                                                $terms_under = get_terms($tax, $args = array(
                                                    'hide_empty' => false, // do not hide empty terms
                                                ));

                                                if (!empty($terms_under) && !is_wp_error($terms_under)) {
                                                    $i = 0;
                                                    // loop through all terms
                                                    foreach ($terms_under as $term_under) {

                                                        // Get the term link
                                                        $term_link = get_term_link($term_under);
                                                        //print_r(get_field('category_rel', 'propiedad_'.$term_under->term_id));
                                                        // if ($term_under->count > 0 and in_array($term->ID, get_field('category_rel'))) {
                                                        if (in_array($term->term_id, get_field('category_rel', $tax . '_' . $term_under->term_id))) {
                                                            if($i==10){
                                                                break;
                                                            }
                                                            // display link to term archive
                                                            echo '<a  class="nav-link color-hover" href="' . home_url() . '/categorias/' . $term->slug . '/?_sft_tipo_de_piel=' . $term_under->slug . '">' . $term_under->name . '</a>';
                                                            $i++;
                                                        }
                                                    }
                                                }
                                                ?>
                                                <a class="cat-see-all" href="<?php echo home_url() ?>/categorias/<?php echo $term->slug ?>">Ver todas <i class="fas fa-angle-right"></i></i></a>
                                    </nav>
                                </div>
                                <div class="col-6 col-sm-3 column">
                                    <nav class="nav flex-column pl-0">
                                        <div class="nav-link mt-3"><b>TIPO DE PRODUCTO</b></div>
                                        <?php
                                                // your taxonomy name
                                                $tax = 'tipo_de_producto';

                                                // get the terms of taxonomy
                                                $terms_under = get_terms($tax, $args = array(
                                                    'hide_empty' => false, // do not hide empty terms
                                                ));

                                                if (!empty($terms_under) && !is_wp_error($terms_under)) {
                                                    $i = 0;
                                                    // loop through all terms
                                                    foreach ($terms_under as $term_under) {

                                                        // Get the term link
                                                        $term_link = get_term_link($term_under);
                                                        //print_r(get_field('category_rel', 'propiedad_'.$term_under->term_id));
                                                        // if ($term_under->count > 0 and in_array($term->ID, get_field('category_rel'))) {
                                                        if (in_array($term->term_id, get_field('category_rel', $tax . '_' . $term_under->term_id))) {
                                                            if($i==10){
                                                                break;
                                                            }
                                                            // display link to term archive
                                                            echo '<a  class="nav-link color-hover" href="' . home_url() . '/categorias/' . $term->slug . '/?_sft_tipo_de_producto=' . $term_under->slug . '">' . $term_under->name . '</a>';
                                                            $i++;
                                                        }
                                                    }
                                                }
                                                ?>
                                                <a class="cat-see-all" href="<?php echo home_url() ?>/categorias/<?php echo $term->slug ?>">Ver todas <i class="fas fa-angle-right"></i></i></a>
                                    </nav>
                                </div>
                                <div class="col-6 col-sm-3 column last">
                                    <nav class="nav flex-column pl-0">
                                        <div class="nav-link mt-3"><b>LOS MÁS VENDIDOS</b></div>
                                        <?php $args = array(
                                                    'post_type' => 'product',
                                                    'meta_key' => 'total_sales',
                                                    'orderby' => 'meta_value_num',
                                                    'taxonomy' => 'product_cat',
                                                    'term' => $term->slug,
                                                    'posts_per_page' => 5
                                                );
                                                $loop = new WP_Query($args);
                                                $i = 0;
                                                while ($loop->have_posts()) : $loop->the_post();
                                                    global $product; 
                                                    if($i==10){
                                                        break;
                                                    }?>
                                                    
                                        <a class="nav-link color-hover" href="<?php the_permalink() ?>"><?php the_title() ?></a>
                                        <?php $i++; endwhile;
                                                ?>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <?php endforeach; ?>
                <li class="nav-item">
                    <a class="nav-link text-uppercase" href="<?php echo home_url() ?>/novedades">novedades</a>
                </li>
                <!-- <li class="nav-item">
                    <a class="nav-link text-uppercase" href="<?php echo home_url() ?>/blog-de-belleza">blog de belleza</a>
                </li> -->
                <?php endif; ?>
                <?php
                wp_reset_postdata();
                wp_reset_query();
                ?>
            </ul>
            <div class="align-self-center col-3">
                <div class="search-field">
                </div>
                <form role="search" method="get" class="search-form" action="<?php echo home_url('/'); ?>">
                    <input id="search-input-menu" type="search" class="form-control" placeholder="Buscar" value="" name="s" title="Search for:" />
                </form>
            </div>
        </div>
    </div>
</nav>