<div class="container mt-4">
    <div class="row">
        <form action="<?php echo site_url() ?>/wp-admin/admin-ajax.php" method="POST" id="filter">
            <div class="form-group">
                <?php
                    if( $terms = get_terms( array(
                        'taxonomy' => 'product_cat', // to make it simple I use default categories
                        'orderby' => 'name'
                    ) ) ) : 
                        // if categories exist, display the dropdown
                        echo '<select name="categoryfilter"><option value="">Select category...</option>';
                        foreach ( $terms as $term ) :
                            echo '<option value="' . $term->term_id . '">' . $term->name . '</option>'; // ID of the category as an option value
                        endforeach;
                        echo '</select>';
                    endif;
                ?>
            </div>
            <div class="form-group">
                <input type="text" name="price_min" placeholder="Min price" />
            </div>
            <div class="form-group">
                <input type="text" name="price_max" placeholder="Max price" />
            </div>
            <div class="form-group">
                <label>
                    <input type="radio" name="date" value="ASC" /> Date: Ascending
                </label>
                <label>
                    <input type="radio" name="date" value="DESC" selected="selected" /> Date: Descending
                </label>
            </div>
            <div class="form-group">
                <label>
                    <input type="checkbox" name="featured_image" /> Only posts with featured images
                </label>
            </div>
            <div class="form-group">
                <button>Apply filter</button>
                <input type="hidden" name="action" value="myfilter">
            </div>
        </form>
        <div id="response"></div>   
    </div>   
</div>