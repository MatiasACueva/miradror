<?php
/**
 * Search results partial template.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>



<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

	<?php if(get_post_type() == 'product'): ?>

		<header class="entry-header my-4">

			<?php get_template_part('woocommerce/inc/cards/product','card'); ?>

		</header><!-- .entry-header -->

	<?php endif; ?>

	<?php if(get_post_type() == 'post'): ?>

	<header class="entry-header my-4">


			<?php get_template_part('woocommerce/inc/cards/news','card'); ?>


	</header><!-- .entry-header -->

	<?php endif; ?>


</article><!-- #post-## -->


