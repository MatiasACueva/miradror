��          L       |       |      }   
   �      �      �   6   �   /  �           .     <     O  <   l   Coupon Code Coupon ID. Coupon code. Select coupon codes {{title}}Coupon Code{{/title}} {{rule /}} {{filter /}} Project-Id-Version: WooCommerce Admin 0.16.0
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/wc-admin
POT-Creation-Date: 2019-07-25 19:51:36+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2019-08-02 15:07+0000
Last-Translator: admin <matias.acueva@gmail.com>
Language-Team: Español
Language: es_ES
Plural-Forms: nplurals=2; plural=n != 1;
X-Poedit-Country: United States
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_x:1,2c;_ex:1,2c;_n:1,2;_nx:1,2,4c;_n_noop:1,2;_nx_noop:1,2,3c;esc_attr__;esc_html__;esc_attr_e;esc_html_e;esc_attr_x:1,2c;esc_html_x:1,2c;
X-Poedit-Basepath: ../
X-Poedit-SearchPath-0: .
X-Poedit-Bookmarks: 
X-Textdomain-Support: yes
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.3.0; wp-5.2.1 Código de cupón ID de cupón. Código de cupón. Seleccione código de cupón {{title}}Código de cupón{{/title}} {{rule /}} {{filter /}} 