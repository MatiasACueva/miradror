<p><b>TIPO DE PIEL</b></p>

<!-- loop taxonomy skin type -->

<div class="custom-control custom-checkbox my-1 mr-sm-2">
    <input type="checkbox" class="custom-control-input" id="todos">
    <label class="custom-control-label" for="todos">Todos</label>
</div>
<?php if( $terms = get_terms( array(
        'taxonomy' => 'tipo_de_piel', // to make it simple I use default categories
        'orderby' => 'name'
    ) ) ) :
    ?>
    <?php	foreach ( $terms as $term ) : ?>
    <div class="custom-control custom-checkbox my-1 mr-sm-2">
        <input type="checkbox" class="custom-control-input" id="<?php echo $term->term_id ?>">
        <label class="custom-control-label" for="<?php echo $term->term_id ?>"><?php echo $term->name ?></label>
    </div>
    <?php	endforeach; ?>
<?php endif; ?>

<!-- end loop taxonomy skin type -->

<!-- loop taxonomy properties -->

<p><b>PROPIEDADES</b></p>


<div class="custom-control custom-checkbox my-1 mr-sm-2">
    <input type="checkbox" class="custom-control-input" id="odos">
    <label class="custom-control-label" for="odos">Todos</label>
</div>
<?php if( $terms = get_terms( array(
        'taxonomy' => 'propiedad', // to make it simple I use default categories
        'orderby' => 'name'
    ) ) ) :
    ?>
    <?php	foreach ( $terms as $term ) : ?>
    <div class="custom-control custom-checkbox my-1 mr-sm-2">
        <input type="checkbox" class="custom-control-input" id="<?php echo $term->term_id ?>">
        <label class="custom-control-label" for="<?php echo $term->term_id ?>"><?php echo $term->name ?></label>
    </div>
    <?php	endforeach; ?>
<?php endif; ?>

<!-- end loop taxonomy properties -->

<!-- loop texture -->

<p><b>Tipo de Producto</b></p>
<div class="custom-control custom-checkbox my-1 mr-sm-2">
    <input type="checkbox" class="custom-control-input" id="dos">
    <label class="custom-control-label" for="dos">Todos</label>
</div>
<?php if( $terms = get_terms( array(
        'taxonomy' => 'tipo_de_producto', // to make it simple I use default categories
        'orderby' => 'name'
    ) ) ) :
    ?>
    <?php	foreach ( $terms as $term ) : ?>
    <div class="custom-control custom-checkbox my-1 mr-sm-2">
        <input type="checkbox" class="custom-control-input" id="<?php echo $term->term_id ?>">
        <label class="custom-control-label" for="<?php echo $term->term_id ?>"><?php echo $term->name ?></label>
    </div>
    <?php	endforeach; ?>
<?php endif; ?>
<div class="mb-5"></div>
</div>

