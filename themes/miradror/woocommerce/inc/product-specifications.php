<!-- Incluido en content-single-product -->
<div class="clearfix"></div>
<?php $description = get_field('product_description'); ?>
<?php $video_url = get_field('product_video'); ?>
<?php $care = get_field('product_care'); ?>
<!-- Description -->

<?php if ($description != '' && $video_url != '' && $care != '') : ?>

<section class="prod-description">

  <?php if ($video_url != '' && $care != ''): ?>

    <ul id="menu-desc" class="nav justify-content-center prod-description-border">

      <?php if ($description != '' ): ?>
                                                      
        <li class="nav-item ">
          <a class="nav-link active prod-description-title" href="#description">DESCRIPCIÓN</a>
        </li>

      <?php   endif; ?>
      
      <?php if ($video_url != '') :  ?>
                                      
      <li class="nav-item">
        <a class="nav-link prod-description-title" href="#aplication">CONSEJOS DE APLICACIÓN</a>
      </li>

      <?php endif; ?>

      <?php if ($care != '') : ?>
                                
      <li class="nav-item">
        <a class="nav-link prod-description-title" href="#care">RUTINA DE CUIDADOS</a>
      </li>

      <?php endif; ?>

      <li class="nav-item">
        <a class="nav-link" href="#help">CONSULTAR</a>
      </li>
    </ul>
    <?php if ($description != '') : ?>

      <div id="description" class="row">
        <?php get_template_part('woocommerce/inc/product', 'description'); ?>
      </div>

    <?php endif; ?>

  <?php endif; ?>

</section>
<?php endif; ?>

<!-- End Description -->

<?php if ($video_url != '') : ?>

<!-- Aplication -->

<section id="aplication">
  <div class="row">
    <div class="col-12 text-center">
      <h3>Consejos de aplicación</h3>
      <?php
      preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $video_url, $match);
      $youtube_id = $match[1];
      ?>
      <iframe width="100%" height="625" src="https://www.youtube.com/embed/<?php echo $youtube_id ?>"></iframe>
    </div>
  </div>
</section>

<!-- End Aplication -->

<?php  endif; ?>


<?php if ($care != '') : ?>

<!-- Care -->

<section id="care">
  <div class="row">
    <div class="col-12 text-center">
      <h3>Rutina de cuidados</h3>
    </div>
    <?php get_template_part('woocommerce/inc/product', 'care'); ?>
  </div>
</section>

<!-- End Care -->

<?php  endif; ?>

<!-- contact-->
<div class="broke-container">
  <?php get_template_part('woocommerce/inc/help'); ?>
</div>

<!-- End contact -->