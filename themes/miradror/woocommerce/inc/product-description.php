<h3 class="col-12 d-md-none">Descripción</h3>

<?php
	if( have_rows('product_description') ):

		while ( have_rows('product_description') ) : the_row();

					// display a sub field value
					$title = get_sub_field('product_description_title');
                    $text = get_sub_field('product_description_text');
                ?>
                
                <div class="col-12 col-lg-4">
                    <h4 class="text-primary"><?php echo $title ?></h4>
                    <p><?php echo $text ?></p>
                </div>
				<?php
		endwhile;
		else :

	endif;
?>