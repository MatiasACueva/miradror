//  -------------------  loop markers  -------------------  //

function initMap() {

    // define latlng center and markers
    //var myLatLng = {lat: -38.01131790573629 , lng: -57.565019614816094};
    var markers = jQuery('.marker');

    //define map
    var mapOptions = {
        zoom: 12,
        mapTypeControl: false,
    };
    var map = new google.maps.Map(document.getElementById('map'), mapOptions);


    var location = "";
    var marcador = "";
    var info = "";
    var bounds = new google.maps.LatLngBounds();
    var infowindow = new google.maps.InfoWindow({
        content: ''
    });

    //loop markers
    markers.each(function () {
        location = {

            lat: jQuery(this).attr('data-lat'),
            lng: jQuery(this).attr('data-lng')

        };

        name = jQuery(this).attr('data-name');
        info = jQuery(this).attr('data-info');

        marcador = new google.maps.LatLng(parseFloat(location.lat), parseFloat(location.lng));

        infowindow = new google.maps.InfoWindow({
            content: info
        });

        // put markers in map
        var punto = new google.maps.Marker({
            map: map,
            position: marcador,
            title: name,
        });
        punto.addListener('click', function () {
            infowindow.open(map, punto);
        });
        bounds.extend(marcador);
    });
    map.panToBounds(bounds);
    map.fitBounds(bounds);
}

//  -------------------  end loop markers  -------------------  //

function goBack() {
    window.history.back();
  }

jQuery("#myNavmenu .searchandfilter ul").click(function(){
    jQuery('#myNavmenu').offcanvas('hide');
});

jQuery("#orderMenu .searchandfilter ul").click(function(){
    jQuery('#orderMenu').offcanvas('hide');
});


//  -------------------  Form contact validation  -------------------  //

(function () {
    'use strict';
    window.addEventListener('load', function () {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function (form) {
            form.addEventListener('submit', function (event) {
                event.preventDefault();
                if (form.checkValidity() === false) {
                    event.stopPropagation();
                    jQuery('#submit').text('ENVIANDO...');
                } else {
                    var formu = jQuery('#contact-form');
                    var formMessages = jQuery('#messages');
                    var formData = jQuery(formu).serialize();

                    jQuery('#submit').text('ENVIANDO...');

                    jQuery.ajax({
                        type: 'POST',
                        url: formu.attr('action'),
                        data: formData
                    }).done(function (resp) {
                        if (resp == 3) {
                            jQuery(formMessages).removeClass('alert alert-success');
                            jQuery(formMessages).addClass('alert alert-danger');
                            jQuery(formMessages).text("Complete todos los campos correctamente");
                        } else if (resp == 2) {
                            jQuery(formMessages).removeClass('alert alert-danger');
                            jQuery(formMessages).addClass('alert alert-success');
                            jQuery(formMessages).text("Su mensaje se envio correctamente");
                        }
                        jQuery('#submit').text('ENVIAR');
                    }).fail(function (data) {
                        jQuery(formMessages).removeClass('alert alert-success');
                        jQuery(formMessages).addClass('alert alert-danger');
                        jQuery(formMessages).text("No se pudo enviar su mensaje");
                        jQuery('#submit').text('ENVIAR');
                    });
                }
                form.classList.add('was-validated');
            }, false);
        });
    }, false);
})();

//  ------------------- End Form contact validation  -------------------  //

//  ------------------- Routine Cards height  -------------------  //

matchColumns=function(){
    var divs,contDivs,maxHeight,divHeight,d;
    divs=document.getElementsByClassName('routine-card');
    contDivs=[];
    maxHeight=0;
    for(var i=0;i<divs.length;i++){
         // make collection with <div> elements with class attribute "equal"
         if(/\bequal\b/.test(divs[i].className)){
               d=divs[i];
               contDivs[contDivs.length]=d;
               if(d.offsetHeight){
                    divHeight=d.offsetHeight;
               }
               else if(d.style.pixelHeight){
                    divHeight=d.style.pixelHeight;
               }
               maxHeight=Math.max(maxHeight,divHeight);
         }
    }
    for(var i=0;i<contDivs.length;i++){
         contDivs[i].style.height=maxHeight + "px";
    }
}
window.onload=function(){
    if(document.getElementsByTagName){
         matchColumns();
    }
}

//  ------------------- End Routine Cards height  -------------------  //

jQuery(document).ready(function () { // Open document ready

    function swiper() {
        var swiper = new Swiper('.swiper-container', {
            slidesPerView: 4,
            spaceBetween: 30,
            centeredSlides: false,
            loop: false,
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
            },
            breakpoints: {
                1024: {
                    slidesPerView: 3,
                    spaceBetween: 20,
                },
                768: {
                    slidesPerView: 2,
                    spaceBetween: 20,
                },
                640: {
                    slidesPerView: 2,
                    spaceBetween: 20,
                },
                500: {
                    centeredSlides: true,
                    loop: true,
                    slidesPerView: 1.2,
                    spaceBetween: 20,
                }
            }
        });
    }

    swiper();
    jQuery(".filter-btn").on("click", function (e) {
        e.preventDefault();
        //LOADING
        jQuery('#ajax-content').css({opacity: 0.5});
        //TRAEMOS EL TERM
        var filter = jQuery(this).data('filter');

        //Borramos la clase active
        jQuery(".filter-btn").removeClass('active');

        //Se la agregamos a este botón
        jQuery(this).addClass('active');

        console.log(filter);

        jQuery.ajax({
            type: 'POST',
            // ESTO TRAE EL ADMIN-AJAX.PHP
            url: dcms_vars.ajaxurl,
            dataType: "html", // add data type
            data: {
                // EN FUNCTIONS.PHP ESTÁ ESTA FUNCION
                action: 'get_ajax_posts',
                //LE PASAMOS PARAMETROS POR POST
                term: filter
            },
            success: function (response) {
                console.log(response);
                //MOSTRAMOS LOS RESULTADOS
                jQuery('#ajax-content').html(response);
                //RECARGAMOS EL SWIPER
                swiper();
                //MOSTRAMOS EL CONTENIDO
                jQuery('#ajax-content').css({opacity: 1});
            }
        });
    });

    jQuery(".megamenu").on("click", function (e) {
        e.stopPropagation();
    });

    //AGREGAMOS LA CLASE SHOW CUANDO HAGO HOVER
    jQuery(".megamenu-li").on("hover", function (e) {
        jQuery('.megamenu').removeClass('show');
        jQuery(this).children('div').addClass('show');
    })
    //cuando sacamos el mouse, saca la clase
    jQuery(".megamenu-li").on("mouseleave", function (e) {
        jQuery('.megamenu').removeClass('show');
    })

    $width = jQuery(window).width();

    if ($width < 1024) {
        jQuery('.add-slide').removeClass('col-4');
        jQuery('.add-slide-prop').removeClass('col-3');
        jQuery('.add-slide').addClass('swiper-slide');
        jQuery('.add-slide-prop').addClass('swiper-slide');
    }
    jQuery(window).resize(function () {
        $width = jQuery(window).width();
        if ($width < 1024) {
            jQuery('.add-slide').removeClass('col-4');
            jQuery('.add-slide-prop').removeClass('col-3');
            jQuery('.add-slide').addClass('swiper-slide');
            jQuery('.add-slide-prop').addClass('swiper-slide');
        } else if ($width > 1024) {
            jQuery('.add-slide').addClass('col-4');
            jQuery('.add-slide-prop').addClass('col-3');
            jQuery('.add-slide').removeClass('swiper-slide');
            jQuery('.add-slide-prop').removeClass('swiper-slide');
        }
    });

//  -------------------  Swiper Variables  -------------------  //

    // ---------  Home Carrousel  --------- //

    var swiper2 = new Swiper('.swiper-carrousel', {
        spaceBetween: 10,
        centeredSlides: true,
        loop: true,
        autoplay: {
            delay: 5000,
            disableOnInteraction: false,
        },
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });


    // ---------  end Home Carrousel  --------- //

    // ---------  News Swiper  --------- //

    var swiper3 = new Swiper('.swiper-news', {
        slidesPerView: 3,
        spaceBetween: 1,
        centeredSlides: false,
        loop: true,
        pagination: {
            el: '.swiper-pagination-news',
            clickable: true,
        },
        breakpoints: {
            1024: {
                slidesPerView: 2,
                spaceBetween: 20,
            },
            768: {
                slidesPerView: 2,
                spaceBetween: 20,
            },
            640: {
                centeredSlides: true,
                slidesPerView: 2,
                spaceBetween: 20,
            },
            500: {
                centeredSlides: true,
                slidesPerView: 1.3,
                spaceBetween: 20,
            }
        }
    });

    // ---------  End News Swiper  --------- //

    // ---------  Category Swiper  --------- //

    var swiper4 = new Swiper('.swiper-category', {
        slidesPerView: 3,
        spaceBetween: 1,
        centeredSlides: true,
        loop: false,
        pagination: {
            el: '.swiper-pagination-cat',
            clickable: true,
        },
        breakpoints: {
            1024: {
                slidesPerView: 2,
                spaceBetween: 10,
            },
            768: {
                slidesPerView: 2,
                spaceBetween: 10,
            },
            640: {
                slidesPerView: 2,
                spaceBetween: 10,
            },
            500: {
                slidesPerView: 1.5,
                spaceBetween: 10,
            }
        }
    });

    // --------- End Category Swiper  --------- //

    // ---------  featured Swiper  --------- //

    var swiper5 = new Swiper('.swiper-featured', {
        slidesPerView: 6,
        spaceBetween: 15,
        centeredSlides: true,
        loop: false,
        breakpoints: {
            1024: {
                slidesPerView: 4,
                spaceBetween: 15,
            },
            768: {
                slidesPerView: 4,
                spaceBetween: 15,
            },
            640: {
                slidesPerView: 6,
                spaceBetween: 1,
            },
            500: {
                slidesPerView: 6,
                spaceBetween: 1,
            }
        }
    });

    // --------- End featured Swiper  --------- //

    // ---------  Care Swiper  --------- //

    var swiper6 = new Swiper('.swiper-care', {
        slidesPerView: 3,
        spaceBetween: 10,
        centeredSlides: true,
        loop: false,
        breakpoints: {
            1024: {
                slidesPerView: 3,
                spaceBetween: 10,
            },
            768: {
                slidesPerView: 2,
                spaceBetween: 20,
            },
            640: {
                slidesPerView: 2,
                spaceBetween: 30,
            },
            500: {
                centeredSlides: false,
                slidesPerView: 1.2,
                spaceBetween: 10,
            }
        }
    });

    // ---------  End Care Swiper  --------- //

    // ---------  Properties Swiper  --------- //

    var swiper7 = new Swiper('.swiper-properties', {
        slidesPerView: 4,
        spaceBetween: 10,
        centeredSlides: true,
        loop: true,
        breakpoints: {
            1024: {
                slidesPerView: 4,
                spaceBetween: 10,
            },
            768: {
                slidesPerView: 3.5,
                spaceBetween: 20,
            },
            640: {
                slidesPerView: 2.5,
                spaceBetween: 15,
            },
            500: {
                slidesPerView: 3.5,
                spaceBetween: 10,
            }
        }
    });

    // ---------  End Properties Swiper  --------- //

//  -------------------  end Swiper Variables  -------------------  //

//  -------------------  Sticky Menu  -------------------  //

var waypoint = new Waypoint({
    element: document.getElementById('description'),
    handler: function() {
    jQuery('.prod-description-border').addClass('fixed-top');
    jQuery('.main-nav-menu').removeClass('fixed-top');
    },
})
var waypoint = new Waypoint({
    element: document.getElementsByClassName('prod-description'),
    handler: function() {
    jQuery('.main-nav-menu').addClass('fixed-top');
    jQuery('.prod-description-border').removeClass('fixed-top');
    },
})


//  -------------------  End Sticky Menu  -------------------  //


}); // close document ready


//  -------------------  Filter Featured Product  -------------------  //

//  -------------------  end Filter Featured Product  -------------------  //
