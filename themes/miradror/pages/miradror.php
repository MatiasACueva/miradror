<?php /* Template Name: Mira Dror */ ?>

<?php get_header( 'shop' ); ?>

<!-- title -->

<section>
    <div class="back-news lab-head">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12 col-md-6">
                    <h2>Mira Dror</h2>
                    <p>Mira Dror es un laboratorio de productos cosméticos con una extensa linea que contiene más de 150 formulaciones exclusivas con el más alto estandar de calidad y elaboradas con ingredientes premium de origen Europeo. Con sede en el partido de Vicente Lopez provincia de Buenos Aires, Mira Dror es lider en innovación y desarrollo en el mercado argentino desde hace 22 años. </p>
                </div>
                <div class="col-md-6 d-none d-md-block">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/img/Miradror_2.png" alt="">
                </div>
            </div>
        </div>
    </div>
</section>

<!-- end title -->

<section id="lab-features" class="text-center text-md-left">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-4 text-center">  
                <img src="<?php echo get_template_directory_uri() ?>/assets/img/miradror_precision.png" alt="">
                <h3 class="text-primary">Alta precisión cosmética</h3>
                <p>Mira Dror es sinónimo de innovación constante y de alta precision cosmética. Desarrollamos fórmulas cosméticas según la Cronobiología aplicada a la piel. Estas fórmulas retrasan los signos del envejecimiento prematuro y resguardan el equilibrio de las funciones metabólicas.</p>
            </div>
            <div class="col-12 col-lg-4 text-center">
                <img src="<?php echo get_template_directory_uri() ?>/assets/img/miradror_premisa.png" alt="">
                <h3 class="text-primary">Premisa Fundamental</h3>
                <p>De Día: Hidratación y protección frente a la agresión ambiental. De Noche: Reparación Celular cuando es máxima actividad metabólica. Mantener la integridad del organismo en convivencia con el medio exterior</p>
            </div>
            <div class="col-12 col-lg-4 text-center">
                <img src="<?php echo get_template_directory_uri() ?>/assets/img/miradror_cronobiologia.png" alt="">
                <h3 class="text-primary">Cronobiología</h3>
                <p>La cronobiología aplicada a la piel da la posibilidad de diferenciar las cremas de día de las cremas de noche. La piel funciona, obedeciendo la sincronizada biología horaria o ciclo circadiano que regula las funciones metabólicas según picos máximos-mínimos durante las <span class="modal-launch" data-toggle="modal" data-target="#exampleModal" target="blank">24 hs.</span></p>

                <!-- Modal -->
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <img class="modal-img" src="<?php echo get_template_directory_uri() ?>/assets/img/cronobiologia.jpg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                               
            </div>
        </div>
    </div>
</section>

<!-- Laboratory Section -->

<?php get_template_part('pages/sections/laboratory'); ?>

<!-- End Laboratory Section -->

<!-- Why us section -->

<section>
    <div class="container">
        <h2>Porque elegirnos</h2>
        <h5>En Mira Dror ante todo nos preocupa la salud cutánea por eso desde nuestros comienzos nuestra prioridad fué desarrollar productos que tengan en cuenta a las pieles más sensibles.</h5>
        <ul>
            <li>Nuestros productos contienen la minima cantidad de fragancias, colorantes a fin de no irritar.</li>
            <li>Libres de parabenos y petrolatos.</li>
            <li>No contienen ningun ingrediente de origen animal por ende son 100% veganos.</li>
            <li>100% Cruelty free</li>
            <li>Contienen vehiculos de altisima calidad "delivery system" que aseguran llevar los Principios Activos al sitio adecuado.</li>
            <li>Libre de TACC.</li>
            <li>Emulsiones oil free  de alta dispersabilidad que otorgan una agradable sensación "after feel".</li>
        </ul>
    </div>
</section>

<!-- End Why us section -->

<!-- News Section -->

<?php get_template_part('pages/sections/news', 'section'); ?>

<!-- End News Section -->

<?php get_footer( 'shop' ); ?>