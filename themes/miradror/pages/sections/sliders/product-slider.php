

    <?php
    // The tax query
    $tax_query[] = array(
        'relation' => 'AND',
        array(
            'taxonomy' => 'product_visibility',
            'field' => 'name',
            'terms' => 'featured',
            'operator' => 'IN', // or 'NOT IN' to exclude feature products
        ),
        array(
            'taxonomy' => 'product_cat',
            'field' => 'slug',
            'terms' => $term
        )
    );
    // The query
    $args = array(
        'post_type' => 'product',
        'order' => 'DES',
        'orderby' => 'menu_order',
        'posts_per_page' => -1,
        'tax_query' => $tax_query // <===
    );


    ?>
    <?php
    query_posts($args);
    if (have_posts()) :
        ?>
        <div class="col-12">
            <h2 class="featured-title">Productos destacados</h2>
        </div>
        <div class="col-12 padding-slider-xl">
            <div class="swiper-container">
                <div class="swiper-wrapper">
                <?php
                    while (have_posts()) :
                        the_post();
                        ?>

            <?php get_template_part('woocommerce/inc/cards/product', 'card');?>

                <?php
                    endwhile;
                    ?>

            </div> <!-- End Swiper Wrapper -->

        <div class="swiper-pagination swiper-pagination-featured"></div>
        </div>
    </div>
    <?php
    else :

    // no rows found

    endif;

    ?>
    <?php wp_reset_query() ?>


