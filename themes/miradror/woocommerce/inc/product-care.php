<?php 	$total = 0; 
		$to_cart = array();
?>
<div class="swiper-care">
    <div class="swiper-wrapper">
<?php
	if( have_rows('product_care') ):
		$i=1;
		
		while ( have_rows('product_care') ) : the_row();

					// display a sub field value
					$action = get_sub_field('product_care_action');
					$description = get_sub_field('product_care_description');
					$posts = get_sub_field('product_care_product');
				?>
				<div class="add-slide d-block product-care col-4 <?php if($i == 1): echo 'first'; endif; ?>">
						<div class="row justify-content-center product-care-head routine-card equal">
							<div class="col-2">
								<p class="count-routine"><?php echo $i ?></p>
							</div>
							<div class="col-10">
								<h5 class="card-title"><?php echo $action; ?></h5>
								<p class="card-text"><?php echo $description ?></p>
							</div>
						</div>
						<?php 
						if( $posts ): ?>
							<?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) 
								setup_postdata($post);
								$_product = wc_get_product( get_the_ID() );
								$price = $_product->get_price(); 
								$total = $total + $price; 
								$id = $_product->get_id();
								$to_cart[] = $id;

								get_template_part('woocommerce/inc/cards/product','card'); 
								
							endforeach; 
							wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly 
						endif; ?>
				</div>				
				<?php
				$i++;
		endwhile;
	endif;
?>
	</div>
</div>

<div class="col-12">
	<a href="?add-to-cart=<?php foreach($to_cart as $id): echo $id;?>,<?php endforeach;?> " class="btn btn-primary d-none d-md-block py-3 mt-3">COMPRAR EL TRATAMIENTO COMPLETO $<?php echo $total ?></a>
	<a href="?add-to-cart=<?php foreach($to_cart as $id): echo $id;?>,<?php endforeach;?> " class="btn btn-primary d-block d-md-none py-3 mt-3">COMPRAR COMPLETO $<?php echo $total ?></a>
</div> 


