        <!-- Nav menu -->
        <div class="separator-nav-sm d-block d-lg-none"></div>
        <header class="d-block d-lg-none navbar navbar-light bg-primary fixed-top">

            <div class="row justify-content-between nav-menu-sm">
                <div class="row">
                    <button class="navbar-toggler nav-button" type="button" data-toggle="offcanvas" data-target="#Navmenu" data-canvas="body">
                        <img src="<?php echo get_template_directory_uri() ?>/assets/img/nav-menu-icon.png" alt="">
                    </button>

                    <div class="menu-item menu-item-type-post_type menu-item-object-page nav-item">
                        <a class="nav-link" data-toggle="collapse" href="#searchbar" role="button" aria-expanded="false" aria-controls="searchbar"><img src="<?php echo get_template_directory_uri() ?>/assets/img/search.png" alt=""></a>
                    </div>
                </div>

                <a class="navbar-brand menu-logo" rel="home" href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" itemprop="url"><img src="<?php echo get_template_directory_uri() ?>/assets/img/logo_w.svg" alt=""></a>

                <div class="row">
                    <div class="menu-item menu-item-type-post_type menu-item-object-page nav-item count-cart-container">
                        <a title="cart" href="<?php echo home_url() ?>/carrito" class="nav-link"><img src="<?php echo get_template_directory_uri() ?>/assets/img/bag.png" alt=""></a>
                        <?php $total_cart = WC()->cart->get_cart_contents_count();
                        if($total_cart != 0){?>
                        <p class="count-cart"><?php echo $total_cart; ?></p>
                        <?php } ?>
                    </div>
                    <div class="menu-item menu-item-type-post_type menu-item-object-page nav-item">
                        <a title="Mi cuenta" href="<?php echo home_url() ?>/ingresar/" class="nav-link my-account"><img src="<?php echo get_template_directory_uri() ?>/assets/img/account.png" alt=""></a>
                    </div>
                </div>
            </div>
            <div class="collapse" id="searchbar">
                <div class="search-field">
                </div>
                <form role="search" method="get" class="search-form" action="<?php echo home_url('/'); ?>">
                    <input id="search-input" type="search" class="form-control" placeholder="Buscar" value="" name="s" title="Search for:" />
                </form>
            </div>
        </header>


        <!-- Nav menu end -->

        <!-- Nav menu first level -->

        <nav id="Navmenu" class="navmenu navmenu-default navmenu-fixed-left offcanvas" role="navigation">
            <a class="navmenu-brand" href="#"><img src="<?php echo get_template_directory_uri() ?>/assets/img/logo.svg" alt=""></a>

            <ul class="nav navmenu-nav flex-column">
                <a class="nav-links" href="<?php echo home_url() ?>/ingresar">
                    <li class="nav-item nav-borders" data-canvas="body">Mi Cuenta</li>
                </a>
                <a class="nav-links" href="#">
                    <li class="nav-item nav-borders" data-canvas="body">Productos</li>
                </a>

                <!-- Loop by categories -->

                <?php if ($terms = get_terms(array(
                    'taxonomy' => 'product_cat',
                    'orderby' => 'name'
                ))) :
                    ?>
                <?php
                    foreach ($terms as $term) : ?>
                <?php $category_name = $term->slug; ?>

                <a class="nav-links" href="<?php echo site_url() ?>/categorias/<?php echo $category_name ?>" role="button" aria-haspopup="true" aria-expanded="false">
                    <li class="nav-item nav-borders nav-categories" data-canvas="body" data-toggle="offcanvas" data-target="#<?php echo $category_name ?>"><?php echo $term->name ?> <i class="fas fa-chevron-right arrow-right"></i></li>
                </a>

                <?php endforeach; ?>
                <?php endif; ?>
                <?php
                wp_reset_postdata();
                wp_reset_query();
                ?>

                <!-- Loop by categories End -->

                <a class="nav-links" href="<?php echo home_url() ?>/novedades">
                    <li class="nav-item nav-borders" data-canvas="body">Novedades</li>
                </a>
                <a class="nav-links" href="<?php echo home_url() ?>/blog-de-belleza">
                    <li class="nav-item nav-borders" data-canvas="body">Blog de belleza</li>
                </a>
                <a class="nav-links" href="<?php echo home_url() ?>/puntos-de-venta">
                    <li class="nav-item nav-borders" data-canvas="body">Puntos de venta</li>
                </a>
                <a class="nav-links" href="<?php echo home_url() ?>/miradror">
                    <li class="nav-item nav-borders" data-canvas="body">Miradror</li>
                </a>
                <a class="nav-links" href="<?php echo home_url() ?>/contacto">
                    <li class="nav-item nav-borders-last" data-canvas="body">Contacto</li>
                </a>
            </ul>

        </nav>

        <!-- Nav menu first level end -->

        <!-- Nav menu Categories -->

        <?php if ($terms = get_terms(array(
            'taxonomy' => 'product_cat',
            'orderby' => 'name'
        ))) :
            ?>
        <?php
            foreach ($terms as $term) : ?>
        <?php $category_name = $term->slug; ?>

        <nav id="<?php echo $category_name ?>" class="navmenu navmenu-default navmenu-fixed-left offcanvas" data-exclude="#Navmenu" role="navigation">
            <a class="navmenu-brand" href="#"><img src="<?php echo get_template_directory_uri() ?>/assets/img/logo.svg" alt=""></a>

            <ul class="nav navmenu-nav flex-column">
                <li class="nav-item nav-borders nav-links">Propiedades</li>

                <?php
                        // your taxonomy name
                        $tax = 'propiedad';

                        // get the terms of taxonomy
                        $terms_under = get_terms($tax, $args = array(
                            'hide_empty' => false, // do not hide empty terms
                        ));

                        if (!empty($terms_under) && !is_wp_error($terms_under)) {

                            // loop through all terms
                            foreach ($terms_under as $term_under) {

                                // Get the term link
                                $term_link = get_term_link($term_under);
                                //print_r(get_field('category_rel', 'propiedad_'.$term_under->term_id));
                                // if ($term_under->count > 0 and in_array($term->ID, get_field('category_rel'))) {

                                if (in_array($term->term_id, get_field('category_rel', $tax . '_' . $term_under->term_id))) {
                                    // display link to term archive
                                    echo '<a class="nav-links" href="' . home_url() . '/categorias/' . $term->slug . '/?_sft_propiedad=' . $term_under->slug . '"><li class="nav-item nav-borders nav-categories">' . $term_under->name . '</li></a>';
                                }
                            }
                        }
                        ?>

                <li class="nav-item nav-borders nav-links">Tipo de Piel</li>

                <?php
                        // your taxonomy name
                        $tax = 'tipo_de_piel';

                        // get the terms of taxonomy
                        $terms_under = get_terms($tax, $args = array(
                            'hide_empty' => false, // do not hide empty terms
                        ));

                        if (!empty($terms_under) && !is_wp_error($terms_under)) {

                            // loop through all terms
                            foreach ($terms_under as $term_under) {

                                // Get the term link
                                $term_link = get_term_link($term_under);
                                //print_r(get_field('category_rel', 'propiedad_'.$term_under->term_id));
                                // if ($term_under->count > 0 and in_array($term->ID, get_field('category_rel'))) {
                                if (in_array($term->term_id, get_field('category_rel', $tax . '_' . $term_under->term_id))) {
                                    // display link to term archive
                                    echo '<a class="nav-links" href="' . home_url() . '/categorias/' . $term->slug . '/?_sft_propiedad=' . $term_under->slug . '"><li class="nav-item nav-borders nav-categories">' . $term_under->name . '</li></a>';
                                }
                            }
                        }
                        ?>

                <li class="nav-item nav-borders nav-links">Tipo de Producto</li>

                <?php
                        // your taxonomy name
                        $tax = 'tipo_de_producto';

                        // get the terms of taxonomy
                        $terms_under = get_terms($tax, $args = array(
                            'hide_empty' => false, // do not hide empty terms
                        ));
                        if (!empty($terms_under) && !is_wp_error($terms_under)) {

                            // loop through all terms
                            foreach ($terms_under as $term_under) {

                                // Get the term link
                                $term_link = get_term_link($term_under);
                                //print_r(get_field('category_rel', 'propiedad_'.$term_under->term_id));
                                // if ($term_under->count > 0 and in_array($term->ID, get_field('category_rel'))) {
                                if (in_array($term->term_id, get_field('category_rel', $tax . '_' . $term_under->term_id))) {
                                    // display link to term archive
                                    echo '<a class="nav-links" href="' . home_url() . '/categorias/' . $term->slug . '/?_sft_propiedad=' . $term_under->slug . '"><li class="nav-item nav-borders nav-categories">' . $term_under->name . '</li></a>';
                                }
                            }
                        }
                        ?>

                <a class="nav-links" href="#">
                    <li class="nav-item nav-borders" data-toggle="offcanvas" data-target="#Navmenu" data-canvas="body"><i class="arrow fas fa-chevron-left"></i> Atras</i></li>
                </a>
            </ul>
        </nav>


        <?php endforeach; ?>
        <?php endif; ?>
        <?php
        wp_reset_postdata();
        wp_reset_query();
        ?>

        <!-- Nav menu Categories End -->