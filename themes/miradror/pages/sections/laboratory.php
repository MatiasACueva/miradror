<section class="laboratory">
    <!--<div class="overlay"></div>
    <img src="<?php echo get_template_directory_uri() ?>/assets/img/lab-b.jpg" alt="">-->
    <div class="image-cover-lab" style="background-image:url('<?php echo get_template_directory_uri() ?>/assets/img/lab-back.jpg ?>')"></div>
    <div class="container">
        <div class="row">
            <div class="d-none d-md-block col-md-8 col-lg-5 top">
                <h2>Nuestro laboratorio</h2>
                <p>Nuestro método de trabajo: "El sinergismo activo"</p>
                <p>En todos los tratamientos usados con los productos Mira Dror, insistiremos en el uso de Principios Activos en forma totalmnete Sinérgica, vehiculizando los productos uno tras otros sin retirar ninguno (salvo en Body Wash Acqua y los de Limpieza). De esta manera logramos estimular enzimáticamente y desintoxicar, además de producir un efecto anti-estress.</p>
                <p>El singérgico activo, entre sus múltiples ventajas, ofrece un modo de trabajo más rendidor (dado que no hay desperdicios y la dosis de aplicación es mucho menor que en las metodologías de trabajo convencionales), más higiénico y mucho más rápido.</p>
                <a href="<?php echo site_url() ?>/miradror" class="btn btn-outline-primary <?php if( $post->ID == 90) { echo 'd-none'; } ?>">CONOCE MÁS</a>
            </div>
        </div>
    </div>
</section>
<section class="lab-sm">
    <div class="container">
        <div class="row">
            <div class="d-block d-md-none text-center">
                <h2>Nuestro laboratorio</h2>
                <p>Nuestro método de trabajo: "El sinergismo activo"</p>
                <p>En todos los tratamientos usados con los productos Mira Dror, insistiremos en el uso de Principios Activos en forma totalmnete Sinérgica, vehiculizando los productos uno tras otros sin retirar ninguno (salvo en Body Wash Acqua y los de Limpieza). De esta manera logramos estimular enzimáticamente y desintoxicar, además de producir un efecto anti-estress.</p>
                <p>El singérgico activo, entre sus múltiples ventajas, ofrece un modo de trabajo más rendidor (dado que no hay desperdicios y la dosis de aplicación es mucho menor que en las metodologías de trabajo convencionales), más higiénico y mucho más rápido.</p>
                <a href="<?php echo site_url() ?>/miradror" class="btn btn-outline-primary <?php if( $post->ID == 90) { echo 'd-none'; } ?>">CONOCE MÁS</a>
            </div>
        </div>
    </div>
</section>