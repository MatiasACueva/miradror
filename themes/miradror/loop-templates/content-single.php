<?php

/**
 * Single post partial template.
 *
 * @package understrap
 */

if (!defined('ABSPATH')) {
	exit; // Exit if accessed directly.
}
?>
<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
	<div class="text-center">
		<header class="entry-header p-4">
			<?php the_title('<h1 class="entry-title">', '</h1>'); ?>

			<div class="entry-meta">

				<?php /* understrap_posted_on();*/ ?>

			</div><!-- .entry-meta -->
			<img class="mb-3" src="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'large'); ?>" alt="">
		</header><!-- .entry-header -->
		<div class="entry-content">

			<?php the_content(); ?>

			<?php
			wp_link_pages(
				array(
					'before' => '<div class="page-links">' . __('Pages:', 'understrap'),
					'after'  => '</div>',
				)
			);
			?>



		</div><!-- .entry-content -->
	</div>
	<div class="container">
		<div class="row">
			<?php
			$posts = get_field('news_product');

			if ($posts) : ?>
				<div class="col-12">
					<h2>Productos Relacionados</h2>
				</div>
				<?php foreach ($posts as $post) : // variable must be called $post (IMPORTANT) 
						setup_postdata($post);
						$_product = wc_get_product(get_the_ID());
						$price = $_product->get_price();
						$id = $_product->get_id();
						$to_cart[] = $id; ?>

					<div class="col-md-6 col-lg-3 mb-4">

						<?php get_template_part('woocommerce/inc/cards/product', 'card'); ?>

					</div>

			<?php
				endforeach;
				wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly 
			endif; ?>

		</div>
	</div>

</article><!-- #post-## -->