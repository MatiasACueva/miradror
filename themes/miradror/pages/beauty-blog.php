<?php /* Template Name: beauty */ ?>

<?php get_header( 'shop' ); ?>

<?php if ( have_posts() ) {
	while ( have_posts() ) {
        the_post(); 
        
?>

<!-- title -->

<section>
    <div class="back-news">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12 col-md-6">
                    <h2><?php the_title()?></h2>
                    <p><?php the_content()?></p>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- end title -->

<!-- news -->

<section class="news">
    <div class="news-posts">
        <div class="container">
            <div class="row">
                <?php   
                                $args = array(
                                'post_type' => 'post',
                                'posts_per_page' => -1,
                                'post_parent' => 0,
                                'order' => 'DES',
                                'orderby' => 'menu_order'
                                            );
                            
                                query_posts($args);

                                if ( have_posts() ):
                                while ( have_posts() ):
                                    the_post(); ?>  

                                <div class="col-md-6 col-lg-4">
                                    <?php
                                    get_template_part('woocommerce/inc/cards/news','card'); 

                                    // display a sub field value ?>
                                </div>
                                <?php
                                endwhile;

                            else :

                                // no rows found

                            endif;

                            ?>
                            <?php wp_reset_query() ?>
            </div>
        </div>
    </div>
</section>

<!-- end news -->
<?php }} ?>
<?php get_footer(); ?>