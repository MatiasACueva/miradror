<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$container = get_theme_mod( 'understrap_container_type' );
?>

<?php get_template_part( 'sidebar-templates/sidebar', 'footerfull' ); ?>

<footer>
	<div class="container">
		<div class="row justify-content-between">
			<div class="col-12 col-sm-2 logo">
				<a href="#"><img src="<?php echo get_template_directory_uri() ?>/assets/img/logo-mira-dror-boutique-transparente.png" alt=""></a>
			</div>
			<div class="col-12 col-sm-8">
				<div class="social-media text-center text-md-right">
					<a href="tel:+5408003338536"><i class="fas fa-phone" target="_blank"></i> 0800 333 8536</a>
					<a class="whatsapp-lg" href="https://web.whatsapp.com/send?phone=+549115003584" target="_blank"><i class="fab fa-whatsapp"></i> 11 5003584</a>
					<a class="whatsapp-sm" href="whatsapp://send?phone=+549115003584" target="_blank"><i class="fab fa-whatsapp"></i> 11 5003584</a>
					<a href="mailto:hola@miradror.com.ar" target="_blank"><i class="far fa-envelope"></i> hola@miradror.com.ar</a>
					<a class="media-icons" href="https://www.facebook.com/miradrorlaboratorio/" target="_blank"><i class="fab fa-facebook-square"></i></a>
					<a class="media-icons" href="https://www.instagram.com/laboratoriomiradror/?hl=es-la" target="_blank"><i class="fab fa-instagram"></i></a>
					<a class="media-icons" href="https://www.youtube.com/channel/UCrpKAqcp4gaDjCAVnVP1-hg" target="_blank"><i class="fab fa-youtube"></i></a>
				</div>
			</div>
		</div>
		<div class="row justify-content-between media-section">
			<div class="col-12 col-sm-6 menu">
				<a href="<?php echo home_url(); ?>/tienda">PRODUCTOS</a>
				<a href="<?php echo home_url(); ?>/puntos-de-venta/">PUNTOS DE VENTA</a>
				<a href="<?php echo home_url(); ?>/miradror">MIRA DROR</a>
				<a href="<?php echo home_url(); ?>/contacto">CONTACTO</a>
			</div>
			<div class="col-12 col-sm-4 text-center text-sm-right politics menu">
				<a href="<?php echo home_url(); ?>/privacidad">Politica de Privacidad</a>
			</div>
		</div>
	</div>
</footer>


</div><!-- #page we need this extra closing tag here -->

<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAwcYGcFVzk19b80pyuJ_CVJwkapFpJIXc&callback=initMap">
</script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/4.0.0/js/jasny-bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/jquery.waypoints.min.js"></script>


<?php wp_footer(); ?>

</body>

</html>

