<div class="col-4">
    <h2 class="text-uppercase"><?php the_title() ?></h2>
    <p><?php echo get_field( "sellpoint_adress" ); ?></p>
    <p><?php echo get_field( "sellpoint_city" ); ?> - <?php echo get_field( "sellpoint_country" ); ?></p>
    <p>Teléfono: <?php echo get_field( "sellpoint_phone" ); ?></p>
</div>


