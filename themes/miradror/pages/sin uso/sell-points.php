<?php /* Template Name: sellpoints */ ?>

<?php get_header( 'shop' ); ?>
<div class="container">
        <div class="col-12 mt-5">
            <h1>Encontrá nuestros puntos de venta</h1>
        </div>
<!-- map -->


<?php echo do_shortcode('[wp_simple_locator] ') ?>

<!-- <div class="map my-5" id="map" style="height: 350px; width: 100%;">
           <?php
            $args = array(
            'post_type' => 'sellpoint',
            'posts_per_page' => -1,
            'post_parent' => 0,
            'order' => 'ASC',
            'orderby' => 'menu_order'
                                    );
            ?>
            <?php query_posts($args); ?>
            <?php while (have_posts()) : the_post(); ?>
                
                
                
                <?php $location = get_field('sellpoint_map' , get_the_id()); ?>  
                <?php print_r($location) ?> 
                <?php if($location){ ?>             
                <div    id="marker" 
                        class="marker" 
                        data-lat="<?php echo $location['lat']; ?>" 
                        data-lng="<?php echo $location['lng']; ?>" 
                        data-name="<?php the_title() ?>" 
                        data-info="<?php the_title() ?> <br>
                                   <?php echo get_field( "sellpoint_adress" ); ?> <br>
                                   <?php echo get_field( "sellpoint_city" );  ?> <br> 
                                   <?php echo get_field( "sellpoint_country" ); ?><br>  
                                   <?php echo get_field( "sellpoint_phone" ); ?>">
                </div>
                <?php } ?>
        

            <?php endwhile; ?>
            <?php wp_reset_query() ?>

</div>  -->

<!-- end map -->

<!-- sellpoints -->

        <!-- <?php
            $args = array(
            'post_type' => 'sellpoint',
            'posts_per_page' => -1,
            'post_parent' => 0,
            'order' => 'ASC',
            'orderby' => 'menu_order'
                                    );
            ?>
            <?php query_posts($args); ?>
            <?php while (have_posts()) : the_post(); ?>
                <?php get_template_part('pages/sell-point','single'); ?>
            <?php endwhile; ?>
            <?php wp_reset_query() ?> -->

</div>

<!-- end sellpoints -->


<?php get_footer( 'shop' ); ?>