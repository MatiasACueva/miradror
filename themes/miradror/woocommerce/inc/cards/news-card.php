<div class="swiper-slide">
    <div class="card">
        <a href="<?php the_permalink() ?>"><img class="news-image" src="<?php echo get_the_post_thumbnail_url(get_the_ID(),'medium'); ?>" alt=""></a>
        <div class="card-body">
            <h5 class="card-title"><a class="hover-title" href="<?php the_permalink() ?>"><?php the_title() ?></a></h5>
            <p class="card-text"><?php echo the_excerpt() ?></p>
            <a class="news-button" href="<?php the_permalink() ?>">SEGUIR LEYENDO</a>
        </div>
    </div>
</div>


