<section id="home_carrousel">
    <div class="swiper-carrousel">
        <div class="swiper-wrapper">

            <?php
            if (have_rows('carrousel_repeater', 'options')) :

                while (have_rows('carrousel_repeater', 'options')) : the_row();

                    // display a sub field value
                    $select = get_sub_field('carrousel_select');
                    $product = get_sub_field('carrousel_product');
                    $image = get_sub_field('carrousel_background');
                    $title = get_sub_field('carrousel_title');
                    $info = get_sub_field('carrousel_info');
                    $button = get_sub_field('carrousel_button');

                    if ($select == 2) :
                        ?>
            <div class="swiper-slide d-block">
                <div class="image-cover" style="background-image:url('<?php echo $image ?>')"></div>
                <!-- <div class="container-fluid d-block d-md-none">
                                <div class="row">
                                    <img class="w-100" src="<?php echo $image ?>" alt="">
                                </div>
                            </div> -->
                <div class="container h-100 carrousel">
                    <div class="row h-100 align-items-center">
                        <div class="col-md-6 carrousel-sm">
                            <div class="top">
                                <h2 class="carrousel-title"><?php echo $product->post_title ?></h2>
                                <p><?php echo $product->post_content ?></p>
                                <div class="properties">
                                    <div class="properties-lg">
                                        <?php
                                                    if (have_rows('product_properties', $product->ID)) :

                                                        while (have_rows('product_properties', $product->ID)) : the_row();

                                                            // display a sub field value
                                                            $image = get_sub_field('product_properties_image');
                                                            $name = get_sub_field('product_properties_name');
                                                            ?>
                                        <div class="properties-slide text-center">
                                            <div class="properties-img">
                                                <img src="<?php echo $image ?>" alt="">
                                            </div>
                                            <p class="text-primary">
                                                <?php echo $name ?>
                                            </p>

                                        </div>
                                        <?php
                                                        endwhile;
                                                    else :
                                                    endif;
                                                    ?>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <a href="<?php echo get_permalink($product->ID); ?>" class="btn btn-primary">CONOCE MÁS</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
                    else : ?>
            <div class="swiper-slide d-block">
                <div class="image-cover" style="background-image:url('<?php echo $image ?>')"></div>
                <!-- <div class="container-fluid d-block d-md-none">
                                <div class="row">
                                    <img class="w-100" src="<?php echo $image ?>" alt="">
                                </div>
                            </div> -->
                <div class="container h-100 carrousel">
                    <div class="row h-100 align-items-center">
                        <div class="col-md-6 carrousel-sm">
                            <div class="top">
                                <h2 class="carrousel-title"><?php echo $title ?></h2>
                                <p><?php echo $info ?></p>
                                <a href="<?php echo home_url() ?>/novedades" class="btn btn-primary text-uppercase"><?php echo $button ?></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
                    endif;
                endwhile;
            endif;
            ?>
        </div> <!-- End Wrapper-->
        <!-- Add Pagination -->
        <div class="swiper-pagination"></div>
    </div> <!-- End Swiper-carrousel -->
</section>