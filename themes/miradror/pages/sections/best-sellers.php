<section id="best-sellers">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center text-md-left">
                <h2>Los más vendidos</h2>
            </div>

            <div class="padding-slider-xl col-12">

                <?php $args = array(
                    'post_type' => 'product',
                    'meta_key' => 'total_sales',
                    'orderby' => 'meta_value_num',
                    'posts_per_page' => 5
                );
                query_posts($args); ?>


                <div class="swiper-container">
                    <div class="swiper-wrapper">
                        <?php if (have_posts()) :
                            while (have_posts()) :
                                the_post();
                                ?>
                                <?php get_template_part('woocommerce/inc/cards/product', 'card'); ?>

                            <?php
                            endwhile;

                        else :

                            // no rows found

                        endif; ?>

                    </div> <!-- End Swiper Wrapper -->

                    <div class="swiper-pagination"></div>
                </div>

                <?php wp_reset_query() ?>


            </div>
        </div>
    </div>
</section>
