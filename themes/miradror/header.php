<?php

/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package understrap
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

$container = get_theme_mod('understrap_container_type');
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link href="https://fonts.googleapis.com/css?family=Quicksand:400,500,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/swiper.min.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/4.0.0/css/jasny-bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <?php do_action('wp_body_open'); ?>
    <div class="site" id="page">

        <!-- ******************* The Navbar Area ******************* -->
        <div id="wrapper-navbar" itemscope itemtype="http://schema.org/WebSite">

            <a class="skip-link sr-only sr-only-focusable" href="#content"><?php esc_html_e('Skip to content', 'understrap'); ?></a>

            <nav class="d-none d-md-block padding-menu navbar navbar-expand-md navbar-dark bg-primary main-nav-menu fixed-top">

                <?php if ('container' == $container) : ?>
                <div class="container">
                    <?php endif; ?>


                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="<?php esc_attr_e('Toggle navigation', 'understrap'); ?>">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <!-- The WordPress Menu goes here -->
                    <?php wp_nav_menu(
                        array(
                            'theme_location' => 'primary',
                            'container_class' => 'collapse navbar-collapse',
                            'container_id' => 'navbarNavDropdown',
                            'menu_class' => 'navbar-nav',
                            'fallback_cb' => '',
                            'menu_id' => 'main-menu',
                            'depth' => 2,
                            'walker' => new Understrap_WP_Bootstrap_Navwalker(),
                        )
                    ); ?>

                    <!-- Your site title as branding in the menu -->


                    <a class="navbar-brand menu-logo" rel="home" href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" itemprop="url"><img class="logo-menu" src="<?php echo get_template_directory_uri() ?>/assets/img/logo-mira-dror-boutique-transparente-blanco.png" alt=""></a>

                    <!-- end custom logo -->
                    <div class="d-none d-md-block menu-item menu-item-type-post_type menu-item-object-page nav-item">
                        <a title="Ingresar" href="<?php echo home_url() ?>/ingresar/" class="nav-link"><img src="<?php echo get_template_directory_uri() ?>/assets/img/account.png" alt="">
                        <?php if(is_user_logged_in()){
                            echo 'Mi Cuenta';
                            } else {
                                echo 'Ingresar';
                            }?>
                        </a>
                    </div>
                    <div class="d-none d-md-block menu-item menu-item-type-post_type menu-item-object-page nav-item count-cart-container">
                        <a title="cart" href="<?php echo home_url() ?>/carrito" class="nav-link"><img src="<?php echo get_template_directory_uri() ?>/assets/img/bag.png" alt=""></a>
                        <?php $total_cart = WC()->cart->get_cart_contents_count();
                        if($total_cart != 0){?>
                        <p class="count-cart"><?php echo $total_cart; ?></p>
                        <?php } ?>
                    </div>

                    <?php if ('container' == $container) : ?>
                </div><!-- .container -->
                <?php endif; ?>


        </div><!-- #wrapper-navbar end -->


        <!--<div class="separator-nav"></div>
		<nav class="second-nav d-none d-md-block">
			<div class="container">
				<div class="row">
					<ul class="nav nav-pills col-9">
						<?php $taxonomy = 'product_cat';
                        $top_level_terms = get_terms(array(
                            'taxonomy' => $taxonomy,
                            'parent' => '0',
                        ));
                        if ($top_level_terms) :
                            ?>
						<?php
                            foreach ($top_level_terms as $top_level_term) :

                                $top_term_id = $top_level_term->term_id;
                                $top_term_name = $top_level_term->name;
                                $top_term_tax = $top_level_term->taxonomy;

                                ?>

						<li class="nav-item dropdown megamenu-li">
							<a class="nav-link dropdown-toggle text-uppercase" data-toggle="dropdown" href="<?php echo site_url() ?>/categorias/<?php echo $top_level_term->slug ?>" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $top_term_name ?></a>
							<div class="dropdown-menu megamenu">
								<div class="container">
									<div class="row">
										<?php $second_level_terms = get_terms(array(
                                                    'taxonomy' => $top_term_tax,
                                                    'child_of' => $top_term_id,
                                                    'parent' => $top_term_id,
                                                ));
                                                if ($second_level_terms) :
                                                    ?>
										<?php foreach ($second_level_terms as $second_level_term) : ?>
										<div class="col-6 col-sm-3 column">
											<p><b><?php echo $second_level_term->name ?></b></p>

											<?php
                                                            $second_term_id = $second_level_term->term_id;
                                                            $second_term_tax = $second_level_term->taxonomy;

                                                            $third_level_terms = get_terms(array(
                                                                'taxonomy' => $second_term_tax,
                                                                'child_of' => $second_term_id,
                                                                'parent' => $second_term_id,
                                                            ));
                                                            if ($third_level_terms) :
                                                                ?>
											<?php foreach ($third_level_terms as $third_level_term) : ?>

											<a href="<?php echo get_term_link($third_level_term); ?>"><?php echo $third_level_term->name ?></a>

											<?php endforeach; ?>

											<?php endif; ?>
										</div>
										<?php endforeach; ?>

										<?php endif; ?>
										<div class="col-6 col-sm-3 column last">
											<p><b>LOS MÁS VENDIDOS</b></p>
										</div>
									</div>
									<div class="see-all">
										<a href="<?php echo home_url() ?>/tienda">TODOS LOS PRODUCTOS <i class="fas fa-long-arrow-alt-right"></i></a>
									</div>
								</div>
							</div>
						</li>
						<?php endforeach; ?>
						<?php endif; ?>
						<li class="nav-item">
							<a class="nav-link text-uppercase" href="<?php echo home_url() ?>/novedades">novedades</a>
						</li>
						<li class="nav-item">
							<a class="nav-link text-uppercase" href="<?php echo home_url() ?>/blog-de-belleza">blog de belleza</a>
						</li>
					</ul>
					<div class="align-self-center col-3">
						<div class="search-field">
						</div>
						<form role="search" method="get" class="search-form" action="<?php echo home_url('/'); ?>">
							<input id="search-input" type="search" class="form-control" placeholder="Buscar" value="" name="s" title="Search for:" />
						</form>
					</div>
				</div>
			</div>
        </nav>-->

        <!-- Menu xl -->                                                       

        <?php get_template_part('pages/menus/navmenu', 'xl'); ?>

        <!-- Menu xl End -->  

        <!-- Menu sm --> 

        <?php get_template_part('pages/menus/navmenu', 'sm'); ?>                                                       
        
        <!-- Menu sm End-->    


