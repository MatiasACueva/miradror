
<?php $_product = wc_get_product(get_the_ID()); ?>

<div class="swiper-slide card-product">
    <div class="card card-care ">
        <a href="<?php the_permalink() ?>">
            <div class="card-image" style="background-image:url('<?php echo get_the_post_thumbnail_url(get_the_ID(), 'large'); ?>')"></div>
        </a>
        <div class="card-body routine-card equal">
            <h6 class="card-title"><a class="card-anchor hover-title" href="<?php the_permalink() ?>"><b><?php the_title() ?></b></a></h6>
            <p class="card-text pb-3"><?php echo get_field("product_subtitle", $_product->get_id()); ?></p>
            <?php

            $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";

            if (is_home()) { } else { ?>
            <p class="price">$<?php echo $_product->get_price(); ?></p>
            <?php } ?>
            <?php ?>
            <div class="d-flex justify-content-center prod-btn">
                <a href="<?php the_permalink() ?>" class="btn btn-outline-primary btn-bottom">DESCUBRILO</a>
            </div>
        </div>
    </div>
</div>
